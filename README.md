# Lightburn Front End Challenge (BETA 1.0.0)

Includes complete dev environment with gulp and sass

Tis Responsible with mobile, tablet and desktop

The form has a surprise too!

### Version

1.0.0

## Install Dependencies

```bash
npm install
```

## Compile Sass & Run Dev Server

```bash
npm start
```

Files are compiled into /src
